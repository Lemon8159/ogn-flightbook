from datetime import datetime, timedelta
from time import time
from operator import attrgetter

from utils.device import get_rdevice, AircraftType

TOW_DELTA = 23


class Flight(object):
    def __init__(self, red, address, start_c, start, start_q, stop_c, stop, stop_q, tz, max_alt=None,
                 start_a=0, stop_a=0):
        self.device = get_rdevice(address=address, red=red)
        self.start_c = start_c          # ICAO takeoff code
        self.start_q = start_q          # qfu takeoff
        self.start_a = start_a          # approximate start time interval in s
        self.stop_c = stop_c            # ICAO landing code
        self.stop_q = stop_q            # qfu landing
        self.stop_a = stop_a            # approximate stop time interval in s
        self.tow = None                 # associated flight in case of towing (flight object)
        self.max_alt = max_alt          # max altitude reach during flight
        self.is_towing = False          # flag (display purpose)
        self.sort_time = None           # use for sorted display
        self.tz = tz
        self.warn = False               # warning flag
        self.start_delta = round(self.start_a/120)
        self.stop_delta = round(self.stop_a/120)
        self.start, self.start_hm = self._start_adjust(start)
        self.stop, self.stop_hm = self._stop_adjust(stop)
        self.duration = self.stop - self.start if self.stop and self.start is not None else None
        self.duration_expected = None                   # use for calculate an expected duration in some case
        if self.start is not None:
            self.sort_time = self.start
        else:
            self.sort_time = self.stop

    def _start_adjust(self, initial_start):
        if initial_start is None:
            return None, None
        if self.start_a < 7200:
            adjust = initial_start - self.start_delta*60
            return adjust, datetime.fromtimestamp(adjust, tz=self.tz).strftime('%Hh%M')
        else:
            return initial_start, datetime.fromtimestamp(initial_start, tz=self.tz).strftime('%Hh%M')

    def _stop_adjust(self, initial_stop):
        if initial_stop is None:
            return None, None
        if self.stop_a < 7200:
            adjust = initial_stop - self.stop_delta*60
            return adjust, datetime.fromtimestamp(adjust, tz=self.tz).strftime('%Hh%M')
        else:
            return initial_stop, datetime.fromtimestamp(initial_stop, tz=self.tz).strftime('%Hh%M')

    @property
    def duration_hm(self):
        if self.duration is not None:
            return '{}:{}'.format(*(str(timedelta(seconds=self.duration)).split(':')[:2]))


class Logbook(object):
    def __init__(self, date, code, red, db, tz=None):
        """
        Initiate Logbook object for a given date , OACI airfield code & timezone
        date fmt "yyyy-MM-dd", ICAO code lower or upper case
        :param date: str
        :param code: str
        :param red: redis client
        :param db: sql db object
        :param tz: any time zone SubClass
        """
        self.tz = tz
        test_date = None
        if date is not None:
            date = date.replace('_', '-').replace('/', '-')
            str_num = date.replace('-', '')
            if len(str_num) == 8 and str_num.isnumeric():
                test_date = date
        if test_date is not None:
            self.date = test_date
            datetime_ = datetime.strptime(self.date, '%Y-%m-%d')
        else:
            datetime_ = datetime.now(tz)
            self.date = datetime.strftime(datetime_, '%Y-%m-%d')
        self.a_day = datetime.strftime(datetime_, '%a')
        # compare logbook date creation with logbook's date
        self._date_same = datetime.strftime(datetime.now(tz), '%Y-%m-%d') == self.date
        # get unix time stamp of Logbook creation
        self._ask_time = time()
        self.code = code.upper()
        self.red = red
        self.db = db
        self.flights = list()
        self.rnames = list()        # => list of receivers name

    def create(self):
        rnames_id = list()
        events = self.db.get_events(self.date, self.code)
        buffer = dict()     # buffer for storing take_off event as buffer[addr]=(take_off_timestamp, qfu, code, approx)
        for addr, code, evt, tsp, max_alt, qfu, approx, rname_id in events:
            # add receiver name only if event code == logbook code
            if code == self.code:
                rnames_id.append(rname_id)
            # ensure protection on None value for db migration (approx column was not existing before 08-2021)
            if approx is None:
                approx = 0
            # Landing event
            if evt == 0:
                # Don't take landing event for approx landing time> 4 hours
                if approx >= 14400:
                    continue
                if addr in buffer:
                    # flight with landing and takeoff
                    flight = Flight(red=self.red, address=addr, start_c=buffer[addr][2], start=buffer[addr][0],
                                    start_q=buffer[addr][1], start_a=buffer[addr][3], stop_c=code, stop=tsp,
                                    stop_q=qfu, stop_a=approx, tz=self.tz, max_alt=max_alt)
                    if flight.start_c != self.code:
                        flight.sort_time = flight.stop
                        flight.start_q = None  # delete qfu if external Takeoff
                    if flight.stop_c != self.code:
                        flight.stop_q = None  # delete qfu if external Landing
                    self.flights.append(flight)
                    del buffer[addr]
                else:
                    # flight without previous takeoff
                    flight = Flight(red=self.red, address=addr, start_c=None, start=None, start_q=None, stop_c=code,
                                    stop=tsp, stop_q=qfu, stop_a=approx, tz=self.tz)
                    self.flights.append(flight)
            # Takeoff event
            else:
                if addr in buffer:
                    # flight without previous landing
                    flight = Flight(red=self.red, address=addr, start_c=buffer[addr][2], start=buffer[addr][0],
                                    start_q=buffer[addr][1], start_a=buffer[addr][3], stop_c=None, stop=None,
                                    stop_q=None,  tz=self.tz)
                    self.flights.append(flight)
                buffer[addr] = tsp, qfu, code, approx
        # elements still in buffer: flights without landing
        for addr in buffer:
            flight = Flight(red=self.red, address=addr, start_c=buffer[addr][2], start=buffer[addr][0],
                            start_q=buffer[addr][1], start_a=buffer[addr][3], stop_c=None, stop=None,
                            stop_q=None, tz=self.tz)
            self.flights.append(flight)

        # sort flights by sort_time
        # erase not well detect events/flights or external takeoff & landing
        # calculate expected_duration
        self.flights = self._check_flight(sorted(self.flights, key=attrgetter('sort_time')))

        # try to associate towing flights
        buffer_g = list()
        buffer_t = list()
        buffer_ti = list()
        for flight in self.flights:

            # delete qfu if external Landing
            if flight.stop_c != self.code:
                flight.stop_q = None

            # don't use flight without takeoff or with external takeoff
            if flight.start is None or flight.start_c != self.code:
                continue

            # Do not use flight with too much approximate takeoff time
            if flight.start_a > TOW_DELTA:
                continue

            if flight.device.aircraft_type == AircraftType.GLIDER_MGLIDER:
                buffer_g.append(flight)

            elif flight.device.aircraft_type in (AircraftType.TOW_PLANE, AircraftType.ULTRALIGHT):
                buffer_t.append(flight)
                buffer_ti.append(flight.start)

        for flight in buffer_g:
            # protect against min() on empty sequence
            if len(buffer_ti) == 0:
                break
            value = min(buffer_ti, key=lambda x: abs(x - flight.start))     # Closest start value in potential towing
            idx = buffer_ti.index(value)                                    # corresponding closest index
            delta = abs(flight.start - value)
            # TODO maybe add qfu matching for better detection
            if delta <= TOW_DELTA:
                flight.tow = buffer_t[idx]
                buffer_t[idx].is_towing = True
                buffer_ti.pop(idx)  # remove elements for working buffers
                buffer_t.pop(idx)

        # get the receivers name with uniq receivers id provided in events
        self.rnames = self.db.get_rnames(list(set(rnames_id)))

    def _check_flight(self, flights):
        ret_flights = list()
        erase_expected = dict()
        idx = 0
        for flight in flights:
            duration_check = False if flight.duration is not None and flight.duration < 60 else True
            external_check = False if flight.start_c != self.code and flight.stop_c != self.code else True
            check = duration_check and external_check
            # calculate/set expected duration only for valid flights and when flightbook's date == actual date
            if check:
                if self._date_same and flight.start is not None and flight.stop is None:
                    flight.duration_expected = int(self._ask_time - flight.start)
                    list_erase = erase_expected.get(flight.device.address, list())
                    list_erase.append(idx)
                    erase_expected[flight.device.address] = list_erase
                ret_flights.append(flight)
                idx += 1
        # erase expected duration for same device with consecutive takeoffs without landing
        for addr in erase_expected:
            list_erase = erase_expected[addr]
            # keep just the last duration_expected and delete others
            for idx in list_erase[:-1]:
                ret_flights[idx].duration_expected = None

        return ret_flights
