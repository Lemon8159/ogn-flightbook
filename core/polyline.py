""""
Polyline econding utility.
Description of google maps Polyline encoding algorithm available @:
https://developers.google.com/maps/documentation/utilities/polylinealgorithm
gmaps use 10e5 factor while osm use 10e6
"""""


def encode(factor, prev, current):
    diff = int(round(current * factor) - round(prev * factor))
    diff <<= 1
    if diff < 0:
        diff = ~diff

    res = list()
    while diff >= 0x20:
        res.append(chr((0x20 | (diff & 0x1f)) + 63))
        diff >>= 5

    res.append(chr(diff + 63))
    return ''.join(res)


def polyencode(factor, prev_pos, cur_pos):
    """
    return a base64 polyencode string for the given previous position
    and current position with precision factor (std use 10e5 or 10e6)
    :param factor: int
    :param prev_pos: float tuple (lat, lng)
    :param cur_pos: float tuple (lat, lng)
    :return: str
    """
    prev_lat, cur_lat = prev_pos[0], cur_pos[0]
    prev_lng, cur_lng = prev_pos[1], cur_pos[1]
    return ''.join((encode(factor=factor, prev=prev_lat, current=cur_lat),
                   encode(factor=factor, prev=prev_lng, current=cur_lng)))


if __name__ == "__main__":
    points = [(38.5, -120.2), (40.7, -120.95), (43.252, -126.453)]
    # encode polyline: _p~iF~ps|U_ulLnnqC_mqNvxq`@
    print(polyencode(10**5, (0, 0), (38.5, -120.2)))
    print(polyencode(10**5, (38.5, -120.2), (40.7, -120.95)))
    print(polyencode(10**5, (40.7, -120.95), (43.252, -126.453)))
