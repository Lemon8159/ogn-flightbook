import json
from datetime import datetime, timezone
import time
from io import BytesIO

from setting import PATH_RETENTION
from utils.device import get_rdevice, mapping_address, mapping_addresses

from aerofiles import igc

ALIVE_TIME = 120        # constant (in sec) for beacon alive flag
FLEET_TIME = 46800      # constant (in sec) for beacon alive in fleet map


def convert_dms(value, type_):
    """
    short decimal to DMS position converter
    value: float
    type_: str ('lat' or 'lng')
    """
    if not isinstance(value, float):
        return
    degrees = int(value)
    submin = abs((value - degrees) * 60)
    minutes = int(submin)
    subseconds = abs((submin - minutes) * 60)
    direction = ""
    if type_ == "lng":
        direction = "W" if degrees < 0 else "E"
    elif type_ == "lat":
        direction = "S" if degrees < 0 else "N"

    return '{}° {}\' {:.0f}\'\' {}'.format(degrees, minutes, subseconds, direction)


def address(red, address_, privacy=True):
    """

    :param red: a redis connection object
    :param address_: str beacon address
    :param privacy: boolean, enable privacy
    :return: dict()
    """
    if address_ is None:
        return
    address_ = address_.upper()

    # privacy concerns
    map_address = mapping_address(address=address_, red=red) if privacy else address_

    key = 'BA:{}'.format(map_address)
    fix = red.lindex(key, -1)
    if fix is None:
        return
    fix = json.loads(fix)
    res = dict()
    res['lat'] = fix.get('lat', None)
    res['lng'] = fix.get('lng', None)
    res['lat_dms'], res['lng_dms'] = convert_dms(fix['lat'], 'lat'),  convert_dms(fix['lng'], 'lng')
    res['utc'] = datetime.utcfromtimestamp(fix['tsp']).strftime('%Y-%m-%d @ %H:%M:%S')
    res['tsp'] = fix['tsp']
    res['alive'] = True if fix['tsp'] + ALIVE_TIME > datetime.now().timestamp() else False
    res['track'] = fix.get('track', None)
    res['alt'] = fix.get('alt', None)
    res['gsp'] = fix.get('gsp', None)
    res['clr'] = fix.get('clr', None)
    res['addr'] = address_
    device = get_rdevice(address=address_, red=red)
    res['acft'] = device.aircraft
    res['reg'] = device.registration
    res['cn'] = device.competition
    res['type'] = device.aircraft_type.value

    # TODO part
    res['receiv'] = "#TODO"
    return res


def maddress(red, addresses, privacy=True):
    """

    :param red: a redis connection object
    :param addresses: iterable str beacon address
    :param privacy: boolean, enable privacy
    :return: a list of dict()
    """
    res = list()
    if addresses is None:
        return None
    addresses = [elt.upper() for elt in addresses]

    # privacy concerns
    map_addresses = mapping_addresses(addresses=addresses, red=red) if privacy else addresses

    for addr in map_addresses:
        res.append(address(red=red, address_=addr, privacy=privacy))
    return [elt for elt in res if elt is not None]


def positions(red, addresses, strict=False):
    """
    return positions end ground speed for given addresses list
    :param red:
    :param addresses:
    :param strict:
    :return: list of dicts()
    """
    ret = list()
    if addresses is None:
        return ret
    addresses = [elt.upper() for elt in addresses]
    for address_ in addresses:
        key = 'BA:{}'.format(address_)
        fix = red.lindex(key, -1)
        if fix is None:
            continue
        fix = json.loads(fix)
        d = dict()
        d['lat'] = fix.get('lat', None)
        d['lng'] = fix.get('lng', None)
        d['gsp'] = fix.get('gsp', None)
        if d['gsp'] != 0:
            continue
        if strict:
            geor = red.georadius(name="GEO:L", longitude=d['lng'], latitude=d['lat'],
                                 radius=3, unit="km", withdist=True, count=1, sort='ASC')
            if len(geor) == 0:
                ret.append(d)
        else:
            ret.append(d)

    return ret


def fleet(red, code):
    """

    :param red: a redis connection
    :param code: str, OACI airfield code
    :return: a list of dict()
    """
    if code is None:
        return None
    code = code.upper()
    h = red.hgetall('FLEET:{}'.format(code))
    if h is None:
        return None
    addrs = list()
    del_addrs = list()
    now_ = datetime.now().timestamp()
    for addr in h:
        if int(h[addr]) + FLEET_TIME > now_:
            addrs.append(addr)
        else:
            del_addrs.append(addr)
    # delete redis hash item for next call & for saving memory space
    if len(del_addrs) >= 1:
        red.hdel('FLEET:{}'.format(code), *del_addrs)

    return maddress(red=red, addresses=addrs)


def path(red, address_, start=None, stop=None, privacy=True):
    """
    return dict with path polyline (list of (lat, lng) tuple) for unix timestamp between start and stop.
    dict response also contains "lto": the last takeoff recorded in the stream if this record found in he stream
    0 for start value will give the path since the last takeoff
    negative start value give the path for the n last
    All responses can not be older than PATH_RETENTION (now set to 24 hours for ogn privacy policy compliance)
    :param red: a redis connection
    :param address_: str (upper or lower case)
    :param start: int , unix timestamp (positive, 0 or negative)
    :param stop:  int, unix timestamp
    :param privacy: boolean, enable privacy
    :return: dict()
    """

    if address_ is None:
        return
    time_call = int(time.time())                  # get Unix TimeStamp (s)
    ogn_min = time_call - PATH_RETENTION          # minus 24 h (ogn policy compliance)

    address_ = address_.upper()
    # privacy concerns
    map_address = mapping_address(address=address_, red=red) if privacy else address_

    stream = 'SA:{}'.format(map_address)
    polyline = list()
    lto = None

    # stream elements are like
    # ('1601308809000-0', {'pos':(47.52022, 12.44962), 'a':542})
    # in case of takeoff event
    # ('1601308809000-1', {'lto': 'ICAO', 'pos':(47.52022, 12.44962), 'a':378})

    if start is not None and start <= 0:
        min_ms = ogn_min * 1000
        # read the stream in reverse order
        if start == 0:
            xr = red.xrevrange(name=stream, max='+', min='{}-0'.format(min_ms))
        else:
            xr = red.xrevrange(name=stream, max='+', min='{}-0'.format(min_ms), count=abs(start))
        if xr is None or len(xr) == 0:
            return
        for _, fields in xr:
            if 'pos' in fields:
                polyline.append((json.loads(fields['pos'])))
            if 'lto' in fields:
                lto = fields['lto']
                if start == 0:
                    # found the lto mark in stream => stop the reading
                    break
        # re-order buffer
        polyline.reverse()
        return {'poly': polyline, 'lto': lto}

    elif start is not None:
        min_ms = start * 1000 if start >= ogn_min else ogn_min * 1000
    else:
        min_ms = ogn_min * 1000

    xrange_max = '+' if stop is None else '{}-0'.format(stop*1000)
    xr = red.xrange(name=stream, min='{}-0'.format(min_ms), max=xrange_max)

    if xr is None or len(xr) == 0:
        return

    for _, fields in xr:
        if 'pos' in fields:
            polyline.append((json.loads(fields['pos'])))
        if 'lto' in fields:
            lto = fields['lto']

    return {'poly': polyline, 'lto': lto}


def igc_(red, address_, date, registration=None, aircraft=None, competition=None, start=None, stop=None):
    """
    return igc BytesIO file like, for unix timestamp between start and stop.
    0 for start value will give igc since the last takeoff
    None value for start and stop give igc for all PATH_RETENTION 
    All responses can not be older than PATH_RETENTION (now set to 24 hours for ogn policy compliance)
    :param red: redis client Object
    :param address_: str
    :param date: datetime object
    :param registration: str
    :param aircraft: str
    :param competition: str
    :param start: int
    :param stop: int

    :return: BytesIO
    """
    
    if address_ is None:
        return
    time_call = int(time.time())            # get Unix TimeStamp (s)
    ogn_min = time_call - PATH_RETENTION    # minus 24 h (ogn policy compliance)
    stream = 'SA:{}'.format(address_)
    # stream elements are like
    # ('1601308809000-0', {'pos':(47.52022, 12.44962), 'a':542})
    # in case of takeoff event
    # ('1601308809000-1', {'lto': 'ICAO', 'pos':(47.52022, 12.44962), 'a':378})

    if start is not None and start == 0:
        xr = list()
        for stsp, fields in red.xrevrange(name=stream, max='+', min='{}-0'.format(ogn_min*1000)):
            xr.append((stsp, fields))
            if 'lto' in fields:
                break
        xr.reverse()
    
    elif start is not None:
        min_ms = start * 1000 if start >= ogn_min else ogn_min * 1000
        xrange_max = '+' if stop is None else '{}'.format(stop*1000)
        xr = red.xrange(name=stream, min='{}-0'.format(min_ms), max=xrange_max)
    
    else:
        min_ms = ogn_min * 1000
        xrange_max = '+' if stop is None else '{}'.format(stop*1000)
        xr = red.xrange(name=stream, min='{}-0'.format(min_ms), max=xrange_max)

    if xr is None or len(xr) == 0:
        return

    # create in-memory igc file
    # https://xp-soaring.github.io/igc_file_format/igc_format_2008.html
    f = BytesIO()
    writer = igc.Writer(f)
    writer.write_headers({
        'manufacturer_code': 'OGN',
        'logger_id': 'OGN',
        'date': date,
        'fix_accuracy': 50,
        'glider_type': aircraft if aircraft is not None else '',
        'glider_id': registration if registration is not None else '',
        'firmware_version': '',
        'hardware_version': '',
        'logger_type': 'OGN-FLIGHTBOOK',
        'gps_receiver': 'GENERIC',
        'competition_id': competition if competition is not None else ''
    })
    for stsp, fields in xr:
        if 'pos' in fields and 'a' in fields:
            latlng = json.loads(fields['pos'])
            writer.write_fix(
                # remove the trailing '000-*' part to obtain unix timestamp
                time=datetime.fromtimestamp(int(stsp[:len(stsp)-5]), tz=timezone.utc),
                latitude=latlng[0],
                longitude=latlng[1],
                gps_alt=int(fields['a']),
                valid=True
            )
    return f
